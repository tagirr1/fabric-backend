const mongoose = require('mongoose');
const api = {};

api.listUsers = (User) => (req,res) =>{
     User.find({}, (error, users) => {
       if (error){res.status(401).json({})} else {
         res.status(200).json(users);
      }
     });

};

api.index = (User) => (req, res) => {
  User.find({}, (error, users) => {
    if (error) {res.status(401).json({})} else {
      res.status(200).json(users);
    }
  });
};


module.exports = api;