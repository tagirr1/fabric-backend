const mongoose = require('mongoose'),
      jwt = require('jsonwebtoken'),
      config = require('@config');

const api = {};

api.login = (User) => (req, res) => {
  if (!req.body.username || !req.body.password) res.json({ success: false, message: 'Please, pass a username and password.' });
  else {
      User.findOne({ username: req.body.username }, (error, user) => {
          if (error) throw error;
          if (!user) res.status(401).send({ success: false, message: 'Authentication failed. User not found.' });
          else {
              user.comparePassword(req.body.password, (error, matches) => {
                  if (matches && !error) {
                      const token = jwt.sign({ user }, config.secret);
                      res.json({ success: true, message: 'Token granted',  token });
                  } else {
                      res.status(401).send({ success: false, message: 'Authentication failed. Wrong password.' });
                  }
              });
          }
      });
  }

};



api.signup = (User) => (req, res) => {
    if (!req.body.username || !req.body.password) res.status(400).json({ success: false, message: 'Please, pass a username and password.' });
    else {
        console.log('registration user request');
        console.log(req.user);
        const newUser = new User({
            username: req.body.username,
            password: req.body.password
        });
        newUser.save((error, user) => {
            if (error) return res.status(400).json({ success: false, message:  'Username already exists.' });
            const token = jwt.sign({ user }, config.secret);
            res.json({ success: true, message: 'Account created successfully' ,  token });
        })
    }
};

/*
api.verify = (headers) => {
  if (headers && headers.authorization) {
    const split = headers.authorization.split(' ');
  if (split.length === 2) return split[1];
    else return null;
  } else return null;
};
*/
module.exports = api;
