const mongoose = require('mongoose');
const api = {};
const formidable = require('formidable');
const shortid = require('shortid');

api.getItem = (models) => (req, res) => {

    let id = req.params.id;
    if (id) {                      //change to #ID get
        models.Good.findById(id, (error, good) => {


            if (error){console.log(error)}

            if (!!good) {
                models.User.findOne({_id: good.author}, (er, user) => {
                    if (er) {
                        res.status(200).json(data);
                    } else {
                        let data = {
                            author_id: good.author,
                            author: user.username,
                            image: good.image,
                            title: good.title,
                            description: good.description
                        };
                        res.status(200).json(data);
                    }
                })

            } else {
                res.status(404).json({});
            }

        })

    } else {
        res.status(400).json({success: false, message: 'Id not found' });
    }
};

api.index = (Good) => (req, res) => {
    Good.find({}, (error, goods) => {
        if (error){console.log(error)}
            res.status(200).json(goods);
        })
};

api.new = (Good) => (req, res) => {
    let form = new formidable.IncomingForm();
    form.uploadDir = "../../upload";
    let body ;
    let images = [];

    form.on('fileBegin', function (name, file){
        let file_name = shortid.generate()+file.name;
        file.path = './upload/' + file_name;
        images.push(file_name)
    });

    form.on('file', function (name, file){
        console.log('Uploaded ' + file.name);
        console.log(file.path);
    });

    form.parse(req, function(err, fields, files) {
        body = fields;

        if (!body.title ) res.status(400).json({ success: false, message: 'Не указано название' });
        else {
            let newProduct;

            newProduct= new Good({
                title: body.title,
                author: req.user.id,
                description: body.description,
                pattern: body.pattern,
                params: body.params,
                image: images
            });

            newProduct.save((error, item) => {
                if (error){
                    console.log(error);
                    return res.status(400).json({ success: false, message:  'Error with creation' });
                }
                res.status(200).json({ success: true, message: 'Product created successfully', item: item });
            })
        }
    });
};

api.remove =  (Good) =>(req, res)=> {
    let id = req.params.id;

    if (id) {

        Good.findByIdAndRemove(  id, (error, elem) => {
            if (error) {console.log(error)} else {
                res.status(200).send({ success: true, message: 'Deleted'+elem._id  })
            }
        })
    } else {
        return res.status(403).send({ success: false, message: 'Not found'  })
    }
};


api.addImage = (Good) => (req, res)=> {
    let form = new formidable.IncomingForm();
    form.uploadDir = "../../upload";
    let body ;
    let images = [];

    form.on('fileBegin', function (name, file){  /// 1
        let file_name = shortid.generate()+file.name
        file.path = './upload/' + file_name; //__dirname +
        images.push(file_name)
    });
    form.on('file', function (name, file){   //   2
        console.log('Uploaded ' + file.name);
        console.log(file.path);
    });

    form.parse(req, function(err, fields, files) {  // 3
        fields;
        Good.findOne({_id: fields.id}, (err, item)=>{
            if (err) { res.status(400) } else {
                item.image=[...images, ...item.image]
            }

            item.save((error) => {
                console.log(error);
                if (error) return res.status(400).json({ success: false, message:  'Error with creation' });
                res.status(200).json({ success: true, message: 'Фото сохранены' });
            })

        })


    });
};


api.edit = (Good) =>(req, res)=> {
    if (req.body.id) {
        let id = req.body.id;
        Good.findOne({_id: id}  , (error, elem) => {
            if (error) {console.log(error)} else {
                elem.title = req.body.title||elem.title;
                elem.description = req.body.description||elem.description;
                elem.pattern = req.body.pattern||elem.pattern;
                elem.params = req.body.pattern||elem.params;

                elem.save(function (err, updated) {
                    console.log('Товар отредактирован');
                    res.status(200).send({ success: true, data: updated })
                })
            }
        })
    } else {
        return res.status(403).send({ success: false, message: 'Not found'  })
    }
};

module.exports = api;
