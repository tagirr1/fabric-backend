const passport = require('passport'),
    config = require('@config'),
    models = require('@AppDir/setup');


module.exports = (app) => {
    const api = app.api.good;

    app.route('/items')
        .get(api.index(models.Good))
        .put(passport.authenticate('jwt', config.session), api.new(models.Good));


    app.route('/item/:id')
        .get(api.getItem(models))
        .delete(
            passport.authenticate('jwt', config.session),
            api.remove(models.Good
        )
    );

    app.route('/edit_good')
        .post(passport.authenticate('jwt', config.session), api.edit(models.Good));

    app.route('/add_image')
        .post(passport.authenticate('jwt', config.session), api.addImage(models.Good));

};





