const passport = require('passport'),
    config = require('@config'),
    models = require('@AppDir/setup');

module.exports = (app) => {
    const api = app.api.auth;

    app.route('/')
        .get((req, res) => res.send('Admin API'));

    app.route('/auth')
        .post( api.login(models.User));


    app.route('/signup')
        .post(api.signup(models.User));

};
