const passport = require('passport'),
      config = require('@config'),
      models = require('@AppDir/setup');
module.exports = (app) => {
    const api = app.api.user;


    app.route('/users')
        .get(api.index(models.User, app.get('budgetsecret')));
}
