const mongoose = require('mongoose');

const Schema = mongoose.Schema({
    title: {
        type: String,
        unique: false,
        required: true
    },
    description: {
        type: String,
        unique: false,
        required: false,
        index: false

    },
    author: {
        type: String,
        unique: false,
        required: false
    },
    pattern: {
        type: String,
        index: false

    },
    params: {
        type: String,
        index: false
    },
    image: {
      type: []
    }
});
mongoose.model('Good', Schema);
