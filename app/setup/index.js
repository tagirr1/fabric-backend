const mongoose = require('mongoose'),
     UserModel = require('@Models/user'),
     GoodModel = require('@Models/good');

const models = {
  User: mongoose.model('User'),
  Good: mongoose.model('Good')
};

module.exports = models;
