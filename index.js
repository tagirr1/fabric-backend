require('module-alias/register');
const http = require('http'),
      App = require('@App'),
      Server = http.Server(App),
      PORT = process.env.PORT || 3001,
      LOCAL = '0.0.0.0';
Server.listen(PORT, LOCAL, () =>{ console.log(`AdminAPI running on ${PORT}`);})


