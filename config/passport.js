const PassportJWT = require('passport-jwt'),
      ExtractJWT = PassportJWT.ExtractJwt,
      Strategy = PassportJWT.Strategy,
      config = require('./index.js'),
      models = require('@AppDir/setup');

module.exports = (passport) => {
  const User = models.User;
  const parameters = {
    secretOrKey: config.secret,
    jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken()
  };
  passport.use(new Strategy(parameters, function (payload, done) {

    User.findOne({ _id: payload.user._id }, (error, user) => {
      if (error) {return done(error)}
      if (!user) {return done(null, false)} else{
        return  done(null, user);
      }
    });
  }));
};
